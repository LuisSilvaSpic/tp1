/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp1;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 *
 * @author Luís Silva
 */
public class TP1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner in = new Scanner(System.in);
        String file_name;
        int n_numbers;
        int ch_metodo = 0;
        int ch_bin = 0;

        //Fora de ProgramaString args
        // ------------ FORA DE PROGRAMA ------------
        /*
             String file_name = args[1];
            int n_numbers= Integer.parseInt(args[0]);
            ch_metodo = Integer.parseInt(args[2]);
            ch_bin = Integer.parseInt(args[3]);
         */
        // ------------ END FORA DE PROGRAMA ------------
        // ------------ DENTRO DE PROGRAMA ------------
        System.out.println("Inserir nome de ficheiro");
        file_name = in.next();
        System.out.println("Inserir numero de numeros");
        n_numbers = in.nextInt();

        System.out.println(" ");
        System.out.println("Escolher metodo");
        System.out.println("1-QuickSort ");
        System.out.println("2-BubbleSort");
        ch_metodo = in.nextInt();
        System.out.println("-------------");

        System.out.println("Em que formato estão os números");
        System.out.println("1-Binario ");
        System.out.println("2-Inteiro");

        ch_bin = in.nextInt();

        int[] numbers = new int[n_numbers];
        // ------------ END DENTRO DE PROGRAMA ------------
        if (ch_bin == 1) {
            
                numbers = ReadFileBin(file_name, n_numbers);
                System.out.println(" ");

                for (int i = 0; i < n_numbers; i++) {
                    System.out.print(" " + numbers[i]);
                }
                switch (ch_metodo) {
                    case 1:
                        QuickSort(0, numbers.length - 1, numbers);
                    case 2:
                        BubbleSort(numbers);
                }
                System.out.println("-------------");

        } else if (ch_bin == 2) {

            numbers = ReadFile(file_name, n_numbers);
            for (int j = 0; j < n_numbers; j++) {
                System.out.print(" " + numbers[j]);
            }

            switch (ch_metodo) {
                case 1:
                    QuickSort(0, n_numbers - 1, numbers);
                case 2:
                    BubbleSort(numbers);
            }
            System.out.println("-------------");
        }
        PrintFile(numbers, ch_bin, n_numbers, file_name);

    }

    private static int[] ReadFile(String file_name, int n_numbers) {
        System.out.println("Ficheiro: " + file_name + "\n");
        int[] numbers = new int[n_numbers];

        try {
            System.out.println("log");
            BufferedReader in = new BufferedReader(new FileReader(file_name));
            //System.out.println("log1");
            String str = "";
            str = in.readLine();

            String[] AUX = str.split(" ");
            for (int j = 0; j < n_numbers; j++) {
                //System.out.println("teste1"+AUX[j]);
                numbers[j] = Integer.parseInt(AUX[j]);
            }

            while ((str = in.readLine()) != null) {
                System.out.println(str);
            }
            in.close();
        } catch (IOException e) {
            System.out.println("Erro de entrada\n");
        }
        return numbers;
    }
    private static int[] ReadFileBin(String file_name, int n_numbers) {
        System.out.println("Ficheiro: " + file_name + "\n");
        int[] numbers = new int[n_numbers];

        try {

                Path path = Paths.get(file_name);
                byte[] fileContents = Files.readAllBytes(path);
                int contador = 0;
                
                for (int i = 0; i < fileContents.length; i++) {
                    if (fileContents[i] != 0) {
                        numbers[contador] = fileContents[i];
                        contador++;
                    }
                }
                
        } catch (IOException e) {
            System.out.println("Erro de entrada\n");
        }
        return numbers;
    }


    private static void BubbleSort(int[] matriz) {

        int n = matriz.length;
        for (int i = 0; i < n; i++) {

            for (int j = 0; j < (n - 1); j++) {

                if (matriz[j] > matriz[j + 1]) {

                    int var = matriz[j];
                    matriz[j] = matriz[j + 1];
                    matriz[j + 1] = var;
                }
            }
        }
        for (int j = 0; j < matriz.length; j++) {
            System.out.println(matriz[j]);
        }

    }

    private static void QuickSort(int l, int h, int[] numbers) {
        //l = low index; h= high index; numbers[]= array
        int i, j;
        int x, y;
        i = l;
        j = h;
        //pivot
        x = numbers[(l + h) / 2];
        do {
            while (numbers[i] < x && i < h) {
                i++;
            }
            while (x < numbers[j] && j > l) {
                j--;
            }
            if (i <= j) {
                y = numbers[i];
                numbers[i] = numbers[j];
                numbers[j] = y;
                i++;
                j--;
            }
        } while (i <= j);
        if (l < j) {
            //System.out.print("entrou1");
            QuickSort(l, j, numbers);
        }
        if (i < h) {
            //System.out.print("entrou2");
            QuickSort(i, h, numbers);
        }/*
        System.out.println("teste QuickSort ");
        for (int k = 0; k <= h; k++) {
            System.out.print(" " + numbers[k]);
        }*/
        if (j < 1 && h < i) {
            for (int k = 0; k < numbers.length; k++) {
                System.out.println(numbers[k]);
            }
        }
    }

    private static void PrintFile(int[] numbers, int ch_bin, int n_numbers, String file_name) {
        if (ch_bin == 1) {
            try {
                FileOutputStream fstream = new FileOutputStream(file_name);
                DataOutputStream outputFile = new DataOutputStream(fstream);

                System.out.println("A escrever no ficheiro...");
                for (int i = 0; i < n_numbers; i++) {
                    outputFile.writeInt(numbers[i]);
                }
                outputFile.close();
            } catch (IOException e) {
                System.out.println("Erro de entrada\n");
            }

        }
        else if (ch_bin == 2){
            try {
                PrintWriter outputFile = new PrintWriter(file_name);

                System.out.println("A escrever no ficheiro...");
                for (int i = 0; i < n_numbers; i++) {
                    //outputFile.writeChar(Integer.toString(numbers[i]));
                     outputFile.print(Integer.toString(numbers[i])+" ");
                }
                outputFile.close();
            } catch (IOException e) {
                System.out.println("Erro de entrada\n");
            }
            
        }
    }
}
