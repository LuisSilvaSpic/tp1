package tp1;

/**
 * 
 */
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Random;

/**
 * @author akg
 *
 */
public class DemoAdvanceFileIO {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException, IOException {
		// TODO Auto-generated method stub
		writeBinaryFile();

	}
	
/**
 * This method opens a binary file and writes the contents
 * of an int array to the file.
 */

	private static void writeBinaryFile() throws IOException, FileNotFoundException
	{
		// Create an array of integers
              Random r = new Random();
	      
	      // Open a binary file for output.
	      FileOutputStream fstream =
	                    new FileOutputStream("Numbers.dat");
	      DataOutputStream outputFile = 
	                    new DataOutputStream(fstream);
	      
	      System.out.println("Writing to the file...");
	      
	      // Write the array elements to the binary file.
	      for (int i = 0; i < 10; i++){
                  
              int Low = 1;
              int High = 40;
              int Result = r.nextInt(High-Low) + Low;
	         outputFile.writeInt(Result);
              }

	      // Close the file.      
	      outputFile.close();
	      System.out.println("   ***Done with writing a binary file.");
	}
}
